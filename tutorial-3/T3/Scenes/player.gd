extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)

var velocity = Vector2()

var doubleJump = 0

var jump = preload("res://Assets/kenney_platformercharacters/PNG/Female/Poses/female_jump.png")
var stand = preload("res://Assets/kenney_platformercharacters/PNG/Female/Poses/female_stand.png")
var slide = preload("res://Assets/kenney_platformercharacters/PNG/Female/Poses/female_slide.png")

func get_input():
	velocity.x = 0
	if is_on_floor():
		$Sprite.set_texture(stand)
		doubleJump = 0
	if Input.is_action_just_pressed('up'):
		$Sprite.set_texture(jump)
		if doubleJump != 2:
			velocity.y = jump_speed
		doubleJump += 1
	if Input.is_action_pressed('right'):
		$Sprite.flip_h = false
		velocity.x += speed
	if Input.is_action_pressed('left'):
		$Sprite.flip_h = true
		velocity.x -= speed
	if Input.is_action_just_pressed("ui_select"):
		if Input.is_action_pressed('left'):
			velocity.x -= 5000
		if Input.is_action_pressed('right'):
			velocity.x += 5000
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
